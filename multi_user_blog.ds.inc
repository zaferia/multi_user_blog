<?php
/**
 * @file
 * multi_user_blog.ds.inc
 */

/**
 * Implements hook_ds_view_modes_info().
 */
function multi_user_blog_ds_view_modes_info() {
  $export = array();

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'teaser_no_image_';
  $ds_view_mode->label = 'Teaser (no image)';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['teaser_no_image_'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'title_only';
  $ds_view_mode->label = 'Title Only';
  $ds_view_mode->entities = array(
    'node' => 'node',
    'taxonomy_term' => 'taxonomy_term',
  );
  $export['title_only'] = $ds_view_mode;

  return $export;
}
