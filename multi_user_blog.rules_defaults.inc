<?php
/**
 * @file
 * multi_user_blog.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function multi_user_blog_default_rules_configuration() {
  $items = array();
  $items['rules_email_when_new_content_is_created'] = entity_import('rules_config', '{ "rules_email_when_new_content_is_created" : {
      "LABEL" : "Email when new content is Created",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "content", "creation", "email", "notification" ],
      "REQUIRES" : [ "workbench_moderation", "rules" ],
      "ON" : [ "node_insert" ],
      "IF" : [
        { "contents_current_state" : { "node" : [ "node" ], "moderation_state" : "needs_review" } }
      ],
      "DO" : [
        { "mail_to_users_of_role" : {
            "roles" : { "value" : { "4" : "4" } },
            "subject" : "[site:name] | [node:content-type], [node:title] is awaiting review.",
            "message" : "Hello [user:name]\\r\\n\\r\\n[node:author] just added [node:content-type] [node:title] to the needs review queue.\\r\\n\\r\\n[node:url]\\r\\n\\r\\nRegards,\\r\\n[site:name] Robot\\r\\n[site:slogan]",
            "from" : "[site:mail]"
          }
        },
        { "drupal_message" : { "message" : "Hello [user:name]\\r\\n\\r\\n[node:author] just added [node:content-type] [node:title] to the needs review queue.\\r\\n\\r\\n[node:url]\\r\\n\\r\\nRegards,\\r\\n[site:name] Robot\\r\\n[site:slogan]" } }
      ]
    }
  }');
  $items['rules_email_writer_when_he_she_s_content_is_marked_back_to_draft'] = entity_import('rules_config', '{ "rules_email_writer_when_he_she_s_content_is_marked_back_to_draft" : {
      "LABEL" : "Email writer when he\\/she\\u0027s content is marked back to Draft.",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "content", "email", "publish" ],
      "REQUIRES" : [ "workbench_moderation", "rules" ],
      "ON" : [ "node_update" ],
      "IF" : [
        { "contents_current_state" : { "node" : [ "node" ], "moderation_state" : "draft" } }
      ],
      "DO" : [
        { "mail" : {
            "to" : [ "node:author:mail" ],
            "subject" : "[node:content-type], [node:title] needs some work.",
            "message" : "Hello [node:author],\\r\\n\\r\\nSorry, your [node:content-type], [node:title] was edited and needs some work.\\r\\n\\r\\nThank For the Contribution,\\r\\n[site:name] Robot\\r\\n\\r\\n[site:slogan]",
            "from" : [ "site:mail" ],
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  $items['rules_email_writer_when_he_she_s_content_is_published'] = entity_import('rules_config', '{ "rules_email_writer_when_he_she_s_content_is_published" : {
      "LABEL" : "Email writer when he\\/she\\u0027s content is published",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "content", "email", "publish" ],
      "REQUIRES" : [ "workbench_moderation", "rules" ],
      "ON" : [ "node_update" ],
      "IF" : [
        { "contents_current_state" : { "node" : [ "node" ], "moderation_state" : "published" } }
      ],
      "DO" : [
        { "mail" : {
            "to" : [ "node:author:mail" ],
            "subject" : "[node:content-type], [node:title] has been published.",
            "message" : "Hello [node:author],\\r\\n\\r\\nCongrats, your [node:content-type], [node:title] has been published.\\r\\n\\r\\nThank For the Contribution,\\r\\n[site:name] Robot\\r\\n\\r\\n[site:slogan]",
            "from" : [ "site:mail" ],
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  return $items;
}
