<?php
/**
 * @file
 * multi_user_blog.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function multi_user_blog_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: user-menu:admin/workbench
  $menu_links['user-menu:admin/workbench'] = array(
    'menu_name' => 'user-menu',
    'link_path' => 'admin/workbench',
    'router_path' => 'admin',
    'link_title' => 'My Workbench',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
  );
  // Exported menu link: user-menu:node/add
  $menu_links['user-menu:node/add'] = array(
    'menu_name' => 'user-menu',
    'link_path' => 'node/add',
    'router_path' => 'node/add',
    'link_title' => 'Add Content',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Add Content');
  t('My Workbench');


  return $menu_links;
}
