<?php
/**
 * @file
 * multi_user_blog.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function multi_user_blog_user_default_roles() {
  $roles = array();

  // Exported role: Publisher.
  $roles['Publisher'] = array(
    'name' => 'Publisher',
    'weight' => '3',
  );

  // Exported role: Writer.
  $roles['Writer'] = array(
    'name' => 'Writer',
    'weight' => '2',
  );

  return $roles;
}
